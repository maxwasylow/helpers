from setuptools import setup

setup(
        name='helpers',
        version='1.0',
        description='Helper functions, classes, and scripts for use in other python projects',
        url='https://bitbucket.org/maxwasylow/helpers',
        author='Max Wasylow',
        author_email='max.wasylow@trinitymirror.com',
        license='MIT',
        packages=[
            'helpers'
        ],
        install_requires=[
            'simplejson',
            'pytz',
            'decorator',
            'requests'
        ],
        zip_safe=False
)
