from helpers import timestamp, ts
import threading
import contextlib
from decorator import decorate
from functools import wraps


def timeit(func):
    def timed(*args, **kwargs):
        ts = timestamp(ms=True)
        result = func(*args, **kwargs)
        te = timestamp(ms=True)

        logger.debug('{} ({}, {}) {:02f} sec'.format(func.__qualname__, args, kwargs, (te - ts) / 1000))
        return result
    return timed

meantime_cache = {}

def get_meantime( qualname = None ):
    longest_name = max( [ len( name ) for name in meantime_cache ] )
    longest_times = max( [ len( str( v[ 1 ] ) ) for _, v in meantime_cache.items() ] )
    if qualname is None:
        return '\n'.join( [
                 ( f'Function: {k}{" "*(longest_name - len(k) + 1)} took {meantime_cache[k][0] / meantime_cache[k][1]:06.10f}s '
                   f'on average to run (run {" "*(longest_times - len(str(meantime_cache[k][1])))}{meantime_cache[k][1]} times)' )
                        for k in meantime_cache ] )
    else:
        if qualname in meantime_cache:
            k = qualname
            return 'Function: {k} took {meantime_cache[k][0] / meantime_cache[k][1]}s on average to run (run {meantime_cache[k][1]} times)'
        else:
            return f'Function: {qualname} has not been recorded yet...'


def _meantime( func, *args, **kwargs ):
    t1 = timestamp( precision = True )
    ret = func( *args, **kwargs )
    t2 = timestamp( precision = True )
    if func.__qualname__ in meantime_cache:
        val, num = meantime_cache[func.__qualname__]
        meantime_cache[func.__qualname__] = ( val + ( t2 - t1 ), num + 1 )
    else:
        meantime_cache[func.__qualname__] = ( t2 - t1, 1 )
    return ret


def meantime( func ):
    return decorate( func, _meantime )


def dummyrun(func):
    def wrappedfunc(*args, **kwargs):
        time.sleep(1)
        return
    return wrappedfunc


def type_check( args, specs, necessary = [] ):
    assertions = []
    for necessary_arg in necessary:
        if necessary_arg not in args or args[ necessary_arg ] is None:
            assertions.append( "Missing necessary argument '{}'!!!".format( necessary_arg ) )

    for arg in args:
        for spec in specs:
            if args[ arg ] is not None and ( arg in specs[ spec ] and type( args[ arg ] ) is not spec ):
                assertions.append( "'{}' should be [{}] but is [{}] <{}>!".format( arg, spec.__name__, type( args[ arg ] ), args[ arg ] ) )
    if len( assertions ) > 0:
        raise AttributeError( '\n'.join( assertions ) )


class WouldBlockError( Exception ):
    pass


@contextlib.contextmanager
def non_blocking_lock( lock ):
    if not lock.acquire( blocking = False ):
        raise WouldBlockError
    try:
        yield lock
    finally:
        lock.release()


stats = { 'last_time': 0 }
lock = threading.Lock()


def throttle( rate ):
    def realthrottle( function ):
        def evenmorerealthrottle( *args, **kwargs ):
            global stats, lock
            while True:
                try:
                    if ts(precision = True) <= stats['last_time'] + rate:
                        pass
                    else:
                        with non_blocking_lock( lock ):
                            stats['last_time'] = ts( precision = True )
                        return function( *args, **kwargs )
                except WouldBlockError:
                    pass
        return evenmorerealthrottle
    return lambda function: realthrottle( function )


def argval(func):
    @wraps(func)
    def _func(*a, **kw):
        retval = func(*a, **kw)
        print(f'Args: {a}\nKwargs: {kw}\nReturns: {retval}')
        return retval
    return _func
