from functools import wraps
from typing import Type


def singleton(cls: Type):
    instances = {}
    @wraps(cls)
    def _class(*a, **kw) -> Type:
        if cls not in instances:
            instances[cls] = cls(*a, **kw)
        return instances[cls]
    return _class
