from helpers.helper import timestamp, format_seconds
from helpers.requests import Requests
from helpers import ts, parse_url
from helpers.time import Period

from datetime import datetime
from typing import Union, Optional
import threading
import logging
import time
import json
import re

logger = logging.getLogger(__name__)
requests = Requests()

class Paginator(object):
    REQUEST_TYPES = {
        'GET': requests.get
    }
    MAX_BATCH_SIZE = 3000

    def __init__(self, url, total_count: Optional[int]=None, in_batches: bool=False, batch_size: int=500, request_type: str='GET', period: Optional[str]=None, max_retries: int=5, **kwargs):
        self.requested_count = total_count
        self.in_batches = in_batches
        self.batch_size = batch_size
        self.req_type = request_type
        self.max_retries = max_retries
        self.req = self.REQUEST_TYPES[self.req_type]
        self.url = parse_url(url, 'schema domain slugs')
        self._init_vars()
        params = parse_url(url, 'query', dictionary=True)
        params = dict(params or {}, **kwargs)
        self.params = params
        if period:
            if self.before or self.after:
                raise ValueError(f'period argument overrides other defined before or after')
            self.after = Period(period).to_timestamp(ms=True)
        self._init_params()
        logger.info(f'{self.req_type} {self.url} with params: {self.params}')

    def _init_vars( self ):
        self.last_batch = None
        self._batch = None
        self.returned = 0
        self.history = []

    def _init_params( self ):
        if self.after is None:
            self.after = 0
        if self.before is None:
            self.before = ts(ms = True) + 1
        if self.count is None:
            self.count = self.batch_size

    @property
    def after( self ):
        return self.params.get( 'after' )

    @after.setter
    def after( self, value ):
        self.params[ 'after' ] = value

    @property
    def before( self ):
        return self.params.get( 'before' )

    @before.setter
    def before( self, value ):
        self.params[ 'before' ] = value

    @property
    def count( self ):
        return self.params.get( 'count' )

    @count.setter
    def count( self, value ):
        self.params[ 'count' ] = value

    @property
    def batch( self ):
        return self._batch

    @batch.setter
    def batch( self, value ):
        self.last_batch = self._batch
        self._batch = value

    @property
    def clean_batch(self):
        if self.last_batch is None:
            return self.batch
        else:
            iids = set(item['iid'] for item in self.last_batch)
            rejected_iids = [item['iid'] for item in self.batch if item['iid'] in iids]
            logger.warning(f'Rejecting: {rejected_iids}')
            return [item for item in self.batch if item['iid'] not in iids]

    def update_params(self):
        self.history.append(self.before)
        earliest_timestamp = min(item['lastModifiedTime'] for item in self.batch)
        latest_timestamp = max(item['lastModifiedTime'] for item in self.batch)
        if earliest_timestamp == latest_timestamp:
            if self.clean_batch:
                if self.count != self.MAX_BATCH_SIZE:
                    new_count = min(self.count * 2, self.MAX_BATCH_SIZE)
                    logger.info(f'Page is filled with items of the same lastModifiedTime, but page is not finished... Increasing count param from {self.count} to {new_count}')
                    self.count = new_count
                else:
                    logger.warning(f'Encountered a page filled with {len(self.batch)} items with the same timestamp {earliest_timestamp}!!! Some items are going to be ignored...')
                    self.before = earliest_timestamp - 1
            else:
                logger.info('Received empty batch - stopping...')
                raise StopIteration
        else:
            self.before = earliest_timestamp
            self.count = self.batch_size

    def issue_request(self):
        retries = 0
        while retries <= self.max_retries:
            response = self.req(self.url, params = self.params)
            if response.ok:
                return json.loads(response.content)
            else:
                logger.warning(f'Retrying request {self.req_type} to {self.url} with params {self.params} [{response}]')
                time.sleep(2 ** retries)
                retries += 1
        else:
            logger.error(f'Too many retries {retries} for {self.req_type} to {self.url} with params {self.params} [{response}]')
            raise StopIteration

    def __iter__(self):
        try:
            while self.after <= self.before:
                self.batch = self.issue_request()
                if self.batch:
                    if self.in_batches:
                        yield self.clean_batch
                    else:
                        for item in self.clean_batch:
                            yield item
                            self.returned += 1
                            if self.requested_count is not None and self.returned >= self.requested_count:
                                logger.info(f'Requested count {self.requested_count} met - stopping...')
                                raise StopIteration
                    self.update_params()
                else:
                    logger.info('Received empty batch - stopping...')
                    raise StopIteration
            else:
                logger.info('Reached end of specified period: {self.after}')
        except StopIteration:
            return
