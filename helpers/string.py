from .list import trim_list
from typing import Union, Optional, List
import re
MultilineStr = Union[str,List[str]]


def ltrim_multiline(multi_str: MultilineStr, max_no: Optional[int]=None, char: str=' '):
    tp = type(multi_str)
    split: List[str]
    if tp is list:
        split = multi_str
    elif tp is str:
        split = multi_str.split('\n')
    else:
        raise TypeError(f"Can't trim object of type {tp}")
    lines = []
    for line in split:
        string: str = line
        count: int = 0
        while line and (max_no is None or count < max_no) and line[0] == char:
            line = line[1:]
            count += 1
        lines.append(line)
    return lines


def count_spaces( string ):
    space_count = 0
    for char in string:
        if char == ' ':
            space_count += 1
        else:
            break
    return space_count


def clean_multiline_string( multiline_str ):
    multiline_str = multiline_str.expandtabs( 4 )
    split = multiline_str.split( '\n' )
    split = trim_list( split, trim = '' )
    space_count = count_spaces( split[ 0 ] )
    split = ltrim_multiline( split, space_count )
    return '\n'.join( split )


def split(string: str, delimiters: MultilineStr=' ') -> List[str]:
    regexp: str
    if type(delimiters) is str:
        regexp = delimiters
    else:
        try:
            regexp = '|'.join(list(delimiters))
        except TypeError:
            raise TypeError('Delimiters must be either a string or a different type of iterable!')
    return re.split(regexp, string)


def isdigit(char: str) -> bool:
    try:
        int( char )
        return True
    except ValueError:
        return False

def isalphanum(char: str) -> bool:
    return char.isalpha() or isdigit(char)
