from helpers import JsonDict, JsonList
from typing import Sequence, Dict, Optional, Any
import helpers
import logging
logger = logging.getLogger(__name__)


class Publisher(JsonDict):
    fields: Sequence = ('uuid', 'name', 'settings')
    defaults: Dict[str, Any] = {
        'uuid': None,
        'settings': dict
    }
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        if self.uuid is None:
            self.uuid: str = helpers.md5(self.name)


class SimplifiedPublication(JsonDict):
    fields: Sequence = ('url', 'publisher', 'mode')
    defaults: Dict[str, Any] = {
        'publisher': None,
        'mode': 'smart'
    }

    def expand(self):
        domain: str = helpers.domain(self.url)
        section: Dict[str, Any] = {
            'url': self.url,
            'job': {
                'default': self.mode,
                'entrypoint': self.url
            }
        }
        return Publication.from_dict({
            'name': domain,
            'domain': domain,
            'publisher': self.publisher,
            'sections': [
                section
            ]
        })


class Publication(JsonDict):
    fields: Sequence = ('uuid', 'name', 'domain', 'publisher', 'sections', 'config', 'parent_iid', 'parent_type', 'iid', 'settings')
    defaults: Dict[str, Any] = {
        'uuid': None,
        'config': dict,
        'settings': dict,
        'parent_iid': None,
        'parent_type': None,
        'iid': None
    }
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.sections = SectionList(*self.sections)
        self.uuid: str = helpers.md5(self.domain)



class Section(JsonDict):
    fields = ('name', 'url', 'job', 'settings', 'uuid')
    defaults = {
        'name': None,
        'settings': dict,
        'uuid': None
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.url: str = helpers.domain(self.url, level=None) + helpers.parse_url(self.url, 'slugs params')
        self.uuid: str = helpers.md5(self.url)



class SectionList(JsonList):
    cls = Section

    def delete():
        self._ = []


ID_LEN = 36
ID_CHUNKS_NO = 5
ID_CHUNK_LENS = [8, 4, 4, 4, 12]

def is_valid_id(id_: str) -> bool:
    if type(id_) is not str:
        return False
    if len(id_) != ID_LEN:
        return False
    id_chunks: List[str] = id_.split('-')
    if len(id_chunks) != ID_CHUNKS_NO:
        return False
    if [len(chunk) for chunk in id_chunks] != ID_CHUNK_LENS:
        return False
    return True

# def get_publication(url_data):
#     uuid = helpers.md5(url_data.domain)
#     pub = Publication.get(uuid = uuid)
#     if pub:
#         pub = pub.to_obj()
#     if pub is None:
#         pub = {
#             'uuid': uuid,
#             'domain': url_data.domain,
#             'name': ' '.join([word.capitalize() for word in helpers.url2pub(url_data.base_url)]) if 'feeds.bbci.co.uk' not in url_data.url else 'BBC',
#             'publisher': url_data.publisher,
#             'sections': []
#         }
#     if url_data.publisher is not None and \
#        len({ 'publisher', 'parent_type', 'parent_iid' }.intersection(pub.keys())) != 3:
#         publisher_data = Publisher.get_by_name(url_data.publisher)
#         if publisher_data is None: raise ValueError(f'Can\'t find publisher: {url_data.publisher}!!!')
#         pub['publisher'] = url_data.publisher
#         pub['parent_type'] = 'harvester-publishers'
#         pub['parent_iid'] = publisher_data['iid']
#     return Publication(**pub)
