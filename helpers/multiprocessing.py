from multiprocessing import Process, Manager, cpu_count

class ProcessManager( object ):
    def __init__( self, max_processes = None ):
        if max_processes is None:
            max_processes = cpu_count()
        self.limit = max_processes
        self.ps = []

    def iter( self, target = None, iterator = [], n = None ):
        if n is None:
            for i in iterator:
                if type( i ) is tuple:
                    self.run( target = target, args = i )
                else:
                    self.run( target = target, args = ( i, ) )
        else:
            for i in range( n ):
                self.run( target = target, args = tuple( iterator ) )
        self.join()

    def run( self, target = None, args = (), kwargs = {} ):
        assert target is not None
        p = Process( target = target, args = args, kwargs = kwargs )
        self.ps.append( p )
        p.start()
        if len( self.ps ) >= self.limit:
            self.join()

    def join( self ):
        for p in self.ps:
            p.join()
        self.ps = []
