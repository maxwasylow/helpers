from .files import read, get_dirname
import logging
import re
logger = logging.getLogger(__name__)
_version_regex = r'^\s*__version__\s*=\s*(\"|\')(?P<version>.*)\1'


def get_version(package_name=None, filename=None):
    if package_name is None and filename is None:
        package_name = get_dirname(last=True)
        logger.warning(f'Setting package name to default: {package_name}')
    version_file = filename or f'{package_name}/__init__.py'
    if logger.isEnabledFor(logging.DEBUG):
        logger.debug(f'Searching for version in {version_file}')
    version_str = read(version_file)
    match = re.search(_version_regex, version_str, flags=re.M)
    if match is None:
        raise ValueError("No version number found!")
    return match['version']
