import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

https = requests.Session()
retries = Retry(total=5, backoff_factor=1, status_forcelist=[500, 502, 503, 504])
https.mount('https://', HTTPAdapter(max_retries = retries))

def _restart_session():
    global https, retries
    https = requests.Session()
    retries = Retry(total=5, backoff_factor=1, status_forcelist = [500, 502, 503, 504])
    https.mount('https://', HTTPAdapter(max_retries = retries))


def get(url, *args, **kwargs):
    try:
        response = https.get(url, *args, **kwargs)
        return response
    except ConnectionError:
        _restart_session()
        return get(url, *args, **kwargs)


def post(url, *args, **kwargs):
    try:
        response = https.post(url, *args, **kwargs)
        return response
    except ConnectionError:
        _restart_session()
        return post(url, *args, **kwargs)

def put(url, *args, **kwargs):
    try:
        response = https.put(url, *args, **kwargs)
        return response
    except ConnectionError:
        _restart_session()
        return put(url, *args, **kwargs)


def delete(url, *args, **kwargs):
    try:
        response = https.delete(url, *args, **kwargs)
        return response
    except ConnectionError:
        _restart_session()
        return delete(url, *args, **kwargs)

def head( url, *args, **kwargs ):
    try:
        response = https.head( url, *args, **kwargs )
        return response
    except ConnectionError:
        _restart_session()
        return head( url, *args, **kwargs )
