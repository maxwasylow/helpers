from nltk.stem import SnowballStemmer
from typing import List, Optional, Union
from . import string
import re
CAMEL_CASE_REGEX = r'(?:(?:[A-Z]+|[a-z])[a-z]*)|\d+'
VAR_DELIMITERS = ('-', '_')
languages = {
    'en': 'english',
    'ru': 'russian',
    'it': 'italian'
}
stemmers = {}


def get_stemmer(lang: str) -> SnowballStemmer:
    if lang not in stemmers:
        stemmers[lang] = SnowballStemmer(languages[lang])
    return stemmers[lang]


def past_tense(word: str) -> str:
    words: List[str] = word.split(' ')
    if len(words) > 2:
        raise TypeError(f'{word} doesn\'t look like a verb!')
    if words[0][-1] == 'e':
        words[0] = words[0] + 'd'
    else:
        words[0] = words[0] + 'ed'
    return ' '.join(words)


def singular(word: str, lang: str='en') -> str:
    capital: List[bool] = [char.istitle() for char in word]
    return ''.join(
        [char.upper() if capital else char
            for char, capital in zip(get_stemmer(lang).stem(word), capital)]
    )


def plural(word: str, lang='en') -> str:
    return word + 's'


def camel_case_to_words(camel_var: str, as_list: bool=False) -> string.MultilineStr:
    return list(match for match in re.findall(CAMEL_CASE_REGEX, camel_var))


def var_to_words(var: str, as_list: bool=False) -> str:
    word_lists: List[List[str]] = [camel_case_to_words(word.strip()) for word in string.split(var, delimiters=VAR_DELIMITERS)]
    words: List[str] = [word for lst in word_lists for word in lst]
    if as_list:
        return words
    else:
        return ' '.join(words)


def capitalize_each(string: Union[str, List[str]]) -> str:
    if string is list:
        split = string
    else:
        split = [word.strip() for word in string.split() if word.strip() != '']
    return ' '.join(word.capitalize() for word in split)


def ordinal(no: int) -> str:
    no_str = str(no)
    if no_str[-1] == '1':
        return no_str + 'st'
    elif no_str[-1] == '2':
        return no_str + 'nd'
    elif no_str[-1] == '3':
        return no_str + 'rd'
    else:
        return no_str + 'th'
