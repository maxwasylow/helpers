def self_initialize(*args, **kwargs):
    """
    Class decorator: takes arbitrary arguments and passes it to class 
    to initialize it by calling _initialize class method
    """
    def _self_initialize(_class):
        _class._initialize(*args, **kwargs)
        return _class
    return _self_initialize


class GenericItem(object):
    def __init__(self, **kwargs):
        if not (hasattr(self.__class__, 'obj_keys') and hasattr(self.__class__, 'sec_keys')):
            raise Exception('Class %s inheriting from GenericItem doesn\'t have obj_keys and/or sec_keys specified' % self.__class__.__name__)
        obj_keys = set(self.__class__.obj_keys)
        sec_keys = set(self.__class__.sec_keys)
        mandatory_fields = obj_keys - sec_keys
        for kwarg in obj_keys:
            try:
                setattr(self, kwarg, kwargs[kwarg])
            except KeyError:
                if kwarg not in sec_keys:
                    raise Exception('Couln\'t find mandatory field {} in arguments!'.format(kwarg))

    @classmethod
    def from_json(cls, json):
        return cls(**json)

    def to_json(self):
        return {
            field: (getattr(self, field) if hasattr(self, field) else None) for field in self.__class__.obj_keys
        }
