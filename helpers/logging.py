import logging
DEFAULT: str = 'WARNING'
DEFAULT_LEVEL: int = logging.WARNING


def get_level(level: str=DEFAULT) -> int:
    try:
        return getattr(logging, level)
    except AttributeError:
        return DEFAULT_LEVEL
