# Readability

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

def ari(sentence):
    #print('Beginning ari...')
    if sentence.strip() == '':
        return -100
    sentences = len([x for x in sentence.replace( '. ', '\n' ).split( '\n' ) if x.strip() != ''])
    words = len([x for x in sentence.split() if x.strip() != ''])
    characters = sum(c.isalpha() for c in sentence) + sum(c.isdigit() for c in sentence)
    # print("Sentences: {}, Words: {}, Characters: {}".format(sentences, words, characters))
    return 4.71 * (characters / words) + 0.5 * (words / sentences) - 21.43


# Filtering

def is_about_facebook(sentence):
    return ('facebook' in sentence or 'Facebook' in sentence) and ('join' in sentence or 'Join' in sentence or 'Like' in sentence or 'like' in sentence)

def is_about_cookies(sentence):
    return 'cookie' in sentence or 'Cookie' in sentence

def is_self_referential(sentence):
    return 'this site' in sentence or 'This site' in sentence or 'Copyright (c)' in sentence or 'copyright (c)' in sentence or 'Press Association' in sentence or 'By registering you' in sentence or 'log in to' in sentence or 'Log in to' in sentence

def is_too_short(sentence, length_limit):
    return len( ' '.join( sentence.split() ) ) < length_limit

def is_too_simple(sentence, ari_limit):
    return ari(sentence) < ari_limit

def pre_accept_sentence(sentence):
    #print('-'*160)
    #print("Sentence {}/{} {}".format(i, max_i, i/max_i))
    #print(sentence)
    return_ = True
    message=""
    if is_about_facebook(sentence):
        return_ = False
        message += "Is about facebook!\n"
    if is_about_cookies(sentence):
        return_ = False
        message += "Is about cookies!\n"
    if is_too_short(sentence, 5):
        return_ = False
        message += "Length is only {} characters!\n".format(len(' '.join(sentence.split())))
   # if is_too_simple(sentence, 2):
   #     return_ = False
   #     message += "The complexity is only {}!\n".format(ari(sentence))
    #print('ARI: {}...'.format(ari(sentence)))
    #print(message if not return_ else 'Accepted!')
    #print('-'*160)
    return return_

def post_accept_sentence(sentence):
    if is_too_short(sentence, 25):
        return False
    if is_self_referential(sentence):
        return False
    return True


# Slicing

def find_centre(sentences):
    lens = [len(x) for x in sentences]
    avg = mean(lens)
    first_i = None
    last_i = None
    for i, l in enumerate(lens):
        if first_i is None and l > avg:
            first_i = i
        if l > avg:
            last_i = i + 1
    centre = int(float((last_i + first_i) / 2)) if not (last_i is None or first_i is None) else int(float((len(sentences)) / 2))
    return first_i, last_i, centre
    


def whitespace_ratio(sentence):
    return float(sum(c.isspace() for c in sentence)) / float(len(sentence))

def find_first_odd(sentences, limit, limit_length):
    for i, sentence in enumerate(sentences):
        if whitespace_ratio(sentence) > limit or 'Copyright (c)' in sentence or 'By registering you are agreeing' in sentence or 'Registered Office: ' == sentence[:19]:
            return i
        elif i > 0.5 * len(sentences) and len(sentence) < limit_length:
            return i
    return None


# Main

def article(bs, ari_limit, length_limit, force_no_article=False):
    if type( bs ) is not str:
        body = [x.text.strip() for x in (bs.find('article').find_all('p') if not force_no_article and not bs.find('article') is None else bs.find_all('p')) if x.text.strip() != '' and (x.a is None or x.text != x.a.text)] # deletes paragraphs having only link in them
    else:
        body = [ sentence.strip() for sentence in bs.split( '\n' ) ]
    nbody = []
    for line in body:
        if line != 'Comments':
            nbody.append(line)
        else:
            break
    body = nbody
    body = [sentence.strip() for chunk in body for sentence in chunk.split('. ') if sentence.strip() != '']
    body = [sentence.strip() for chunk in body for sentence in chunk.split('\n') if sentence.strip() != '' and pre_accept_sentence(sentence.strip())]
    cache = {}
    nbody = []
    for sentence in body:
        if not sentence in cache:
            cache[sentence] = 1
            nbody.append(sentence)
    body = nbody
    if len(body) > 3:
        beginning, end, centre = find_centre(body)
    else:
        beginning, end, centre = [0, len(body), int(len(body) / 2)]
    for i in range(centre, beginning, -1):
        if not post_accept_sentence(body[i]):
            beginning = i + 1
            break
    for i in range(centre, end):
        if not post_accept_sentence(body[i]):
            end = i
            break
    body = body[beginning:end]
    nbody = ""
    for i, sentence in enumerate(body):
        if i + 1 != len(body):
            if '.' in sentence[-5:]:
                nbody += sentence + ' '
            else:
                nbody += sentence + '. '
        else:
            nbody += sentence
    if not force_no_article and len(nbody) == 0 and type( bs ) is not str:
        return article(bs, ari_limit, length_limit, True)
    return nbody
