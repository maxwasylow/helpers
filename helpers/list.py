from typing import Any, TypeVar, Sequence, List
T = TypeVar('T')


def trim_list(lst: Sequence[T], trim: T='') -> Sequence[T]:
    while lst[0] == trim:
        lst = lst[1:]
    while lst[-1] == trim:
        lst = lst[:-1]
    return lst

def as_list(arg: Any, tuples: bool=True) -> list:
    list_types = (tuple, list) if tuples else list
    if isinstance(arg, list_types):
        return arg
    else:
        return [arg]
