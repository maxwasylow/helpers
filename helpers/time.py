from .string import isdigit
from .helper import JsonDict
from . import helper as helpers

from typing import Union, Tuple, Dict, Generator, Optional, Any, Callable
from datetime import timedelta, datetime
import threading


class PeriodChunk(object):
    def __init__(self, quant: float, order: str, force: bool=False) -> None:
        order_cfg = JsonDict(**order_config[order])
        if not force:
            while quant >= order_cfg.max:
                order = reverse_lookup[order_cfg.ordering + 1]
                quant = quant / order_cfg.max
                order_cfg = JsonDict(**order_config[order])
            while quant < 1 and order_cfg.ordering != 0:
                order = reverse_lookup[order_cfg.ordering - 1]
                order_cfg = JsonDict(**order_config[order])
                quant = quant * order_cfg.max
        self.value = quant * order_cfg.value
        self.quant = quant
        self.order = order
        self.cfg = order_cfg

    def __str__(self) -> str:
        return f'<PeriodChunk {self.quant}{self.order} {self.value}s>'

    def __repr__(self) -> str:
        return str(self)

    def to_timedelta(self):
        get_kwargs = self.cfg.timedelta
        return timedelta(**get_kwargs(self.quant))

    def to_order(self, order: str) -> 'PeriodChunk':
        if order not in order_config:
            raise ValueError(f"Can't convert to order {order}!")
        if order != self.order:
            ratio = self.cfg.value / order_config[order]['value']
            return self.__class__(self.quant * ratio, order, force=True)
        else:
            return self

    @classmethod
    def parse(cls, period: str):
        quant = ''
        order = ''
        for char in period:
            if isdigit(char) or char == '.':
                if order:
                    yield cls(float(quant), order)
                    quant = ''
                    order = ''
                quant += char
            elif char.isalpha():
                if not quant:
                    raise ValueError(f"Can't parse period string: {period}")
                order += char
            else:
                raise ValueError(f"Invalid character {char} in period string {period}")
        if quant:
            if not order:
                raise ValueError(f"Can't parse period string: {period}")
            yield cls(float(quant), order)

    def __add__(self, other: 'PeriodChunk') -> 'PeriodChunk':
        if self.order == other.order:
            return self.__class__(self.quant + other.quant, self.order)
        else:
            tmp = other.to_order(self.order)
            return self.__class__(self.quant + tmp.quant, self.order)


class Period(object):
    def __init__(self, period: str) -> None:
        self.representation = period
        if period == 'now':
            self.chunks = ['now']
            self.sorted = ['now']
            self.order = None
            self.seconds = 0
        else:
            self.chunks = list(PeriodChunk.parse(period))
            self.sorted = sorted(self.chunks, key=lambda chunk: chunk.value, reverse=True)
            self.order = self.sorted[0].order
            self.seconds = sum(chunk.value for chunk in self.chunks)

    @staticmethod
    def sort_key(period_chunk: Tuple[float, str, int]) -> int:
        return period_chunk[2]

    def to_timedelta(self) -> timedelta:
        return sum((chunk.to_timedelta() for chunk in self.chunks), timedelta())

    def to_timestamp(self, **kw) -> Union[int, float]:
        val: float = helpers.ts(precision=True) - self.seconds
        if kw.get('precision'):
            return val
        elif kw.get('ms'):
            return int(val * 1000)
        else:
            return int(val)

    def fill(self, dt, asdate=False):
        cfg = self.sorted[0].cfg
        if isinstance(dt, (int, float)):
            ts = dt
        else:
            ts = dt_to_ts(dt)
        if self.order == 'w':
            newts = 345600 + ((int((ts - 345600) / cfg.value) + 1) * cfg.value)
        else:
            newts = (int(ts / cfg.value) + 1) * cfg.value
        if asdate:
            return datetime.fromtimestamp(newts)
        else:
            return newts

    def reset(self, dt, asdate=False):
        cfg = self.sorted[0].cfg
        if isinstance(dt, (int, float)):
            ts = dt
        else:
            ts = dt_to_ts(dt)
        if self.order == 'w':
            newts = 345600 + (int((ts - 345600) / cfg.value) * cfg.value)
        else:
            newts = int(ts / cfg.value) * cfg.value
        if asdate:
            return datetime.fromtimestamp(newts)
        else:
            return newts


class Interval(object):
    '''
    Interval object represents an amount of time
        for instance 1 week or 1 month + 3 hours
    '''
    def __init__(self, after: int, before: int, step: Optional[int]=None):
        '''
        Args:
            after:  start of the interval as a UNIX timestamp
            before: end of the interval as a UNIX timestamp
            step:   optional argument to divide the interval into buckets eg. 1 hour intervals in 1 day
        '''
        self.after = after
        self.before = before - 1
        self.step = step

    @classmethod
    def from_str(cls, interval_str: str, before: Optional[str]=None, step: Optional[str]=None, natural: bool=True) -> 'Interval':
        '''
        Creates interval from a string representation
        Args:
            interval_str:   string representation of the interval eg. 1H | 2w1.5d | 15M
            before:         optional argument specifying when does the interval end
            step:
            natural:        optional argument specifying whether interval should start and end
                                on naturally occuring time eg. midnight, or full hour
        Returns:
            Interval: Interval object corresponding to interval_str representation
        '''
        interval_period = Period(interval_str)
        now = helpers.ts()
        if before is not None:
            now = now - Period(before).seconds
        if not natural:
            return cls(now - interval_period.seconds, now, Period(step).seconds)
        else:
            if step is not None:
                now = Period(step).fill(now)
            else:
                now = interval_period.reset(now)
            return cls(now - interval_period.seconds, now, Period(step).seconds)

    def to_start_end(self) -> Tuple[int]:
        return self.after, self.before - 1

    def __contains__(self, o: Any) -> bool:
        if isinstance(o, (int, float)):
            return o < self.before and o > self.after
        if isinstance(o, Interval):
            return o.after >= self.after and o.before < self.before
        else:
            return False

    def __iter__(self):
        if self.step is None:
            yield self
            return
        after, before = self.to_start_end()
        while after <= before:
            yield Interval(after, after + self.step)
            after += self.step
        return


order_config = {
    'M': {
        'ordering': 0,
        'value': 60,
        'timedelta': lambda quant: { 'minutes': quant },
        'max': 60
    },
    'H': {
        'ordering': 1,
        'value': 60*60,
        'timedelta': lambda quant: { 'hours': quant },
        'max': 24
    },
    'd': {
        'ordering': 2,
        'value': 60*60*24,
        'timedelta': lambda quant: { 'days': quant },
        'max': 7
    },
    'w': {
        'ordering': 3,
        'value': 60*60*24*7,
        'timedelta': lambda quant: { 'weeks': quant },
        'max': 4
    },
    'm': {
        'ordering': 4,
        'value': 60*60*24*7*4,
        'timedelta': lambda quant: { 'weeks': 4 * quant },
        'max': float('Infinity')
    }
}

reverse_lookup = {
    0: 'M',
    1: 'H',
    2: 'd',
    3: 'w',
    4: 'm'
}

class TickTock:
    '''
        An object recording two types of events and a distance between them
        Events:
            tick: starts a sequence, all following tock events will
                  calculate an elapsed time to tick as a start point
            tock: continues a sequence, calculates elapsed times since last tock and last tick
        Args:
            format:     print results in formatted/unformatted way          default=True
            print_func: which print handler to default to [eg. logger.info] default=print
            tick:       do the first 'tick' on initialization               default=True
    '''
    def __init__(self, format: bool=True, print_func: Callable=print, tick: bool=True) -> None:
        self.format_f = format
        self.print = print_func
        if tick:
            self.tick()
        else:
            self.start = None
            self.end = None

    def tick(self) -> None:
        '''Record a tick event'''
        self.start = self.last = helpers.ts(precision=True)

    def tock(self, format: Optional[bool]=None, print_func: Optional[Callable]=None) -> Tuple[float, float]:
        '''
            Record a tock event and print out how much time elapsed since last tock and tick
            Args:
                format:     print results in formatted/unformatted way
                print_func: which print handler to use [eg. logger.info]
            Returns:
                Tuple of floats => time since last tock, time since last tick
        '''
        if format is None:
            format = self.format_f
        if print_func is None:
            print_func = self.print
        new_last = helpers.ts(precision=True)
        old_last, self.last = self.last, new_last
        tock_elapsed = new_last - old_last
        tick_elapsed = new_last - self.start
        if format:
            tock_formatted = helpers.format_seconds(tock_elapsed)
            tick_formatted = helpers.format_seconds(tick_elapsed)
            msg = f'Elapsed {tock_formatted} / {tick_formatted} [last tock/last tick]'
        else:
            msg = f'{tock_elapsed}/{tick_elapsed}'
        print_func(msg)
        return tock_elapsed, tick_elapsed


_ticks_per_thread: Dict[int, TickTock] = {}
def tick(format: bool=True, print_func: Callable=print) -> None:
    '''
        Create a TickTock object for a current thread,
        or do a 'tick' for an existing one
        Args: look at helpers.time.TickTock.__init__
    '''
    global _ticks_per_thread
    ident: int = threading.get_ident()
    if ident in _ticks_per_thread:
        _ticks_per_thread[ident].tick()
    else:
        _ticks_per_thread[ident] = TickTock(format=format, print_func=print_func)

def tock(format: bool=True, print_func: Callable=print):
    '''
        Record a new 'tock' for a default TickTock object of a current thread
        [tock without a tick defaults to both tick and tock]
        Args: look at helpers.time.TickTock.tock
    '''
    global _ticks_per_thread
    ident = threading.get_ident()
    if ident not in _ticks_per_thread:
        _ticks_per_thread[ident] = TickTock(format=format, print_func=print_func)
    return _ticks_per_thread[ident].tock()

def dt_to_ts(dt):
    return time.mktime(dt.timetuple())


def to_midnight(dt: datetime):
    '''
    Round the provided datetime object to the last day
    Usage:
        >>> to_midnight(datetime.strptime('14 Aug 1993 10:21', '%d %b %Y %H:%M')).strftime('%d %b %Y %H:%M')
        "14 Aug 1993 00:00"
    '''
    return dt.replace(hour=0, minute=0, second=0, microsecond=0)

second = 1000
minute = 60 * second
hour = 60 * minute
day = 24 * hour
week = 7 * day
