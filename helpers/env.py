from typing import List, Optional, Sequence
import sys
import os

VALID_ENVS: Sequence[str] = ('test', 'stag', 'prod')


def get_env() -> str:
    env: Optional[str]
    env = get_env_from_args()
    if env is None:
        env = get_env_from_vars()
    if env is not None:
        if is_valid_env(env):
            return env
        else:
            raise ValueError(f'Resolved environment {env} is not valid!')
    else:
        raise RuntimeError('Could not resolve environment!')

def get_env_from_args() -> Optional[str]:
    args: List[str] = sys.argv
    env = None
    if '-e' in args:
        env = args[args.index('-e') + 1]
    elif '--env' in args:
        env = args[args.index('--env') + 1]
    else:
        for arg in args:
            if arg in VALID_ENVS:
                env = arg
    return env

def get_env_from_vars() -> Optional[str]:
    env = os.getenv('ENVIRONMENT')
    if env is not None:
        return env
    return os.getenv('ENV')

def is_valid_env(env: str) -> bool:
    return env in VALID_ENVS
