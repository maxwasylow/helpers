from typing import Sequence, Any, Type, Union, Optional, Dict
from .patterns import singleton
from functools import wraps
import logging
logger = logging.getLogger(__name__)
BindingKey = Union[Type, str]


@singleton
class Injector(object):
    def __init__(self, bindings: Optional[Dict[BindingKey, Any]]=None):
        if bindings is None:
            bindings = {}
        self.bindings: Dict[BindingKey, Any] = bindings

    def bind(self, variable: Any, key: Optional[BindingKey]=None):
        if key is None:
            key = type(variable)
        if key in self.bindings:
            logger.warning(f'{ofType} already exists within bindings and will be overwritten!')
        self.bindings[key] = variable

    def get(self, key: BindingKey) -> Any:
        if key in self.bindings:
            return self.bindings[key]
        raise TypeError(f"{key} wasn't bound but was tried to be accessed!")


def inject(*dependencies):
    def wrapper(func):
        @wraps(func)
        def _func(*a, **kw):
            injector = Injector()
            deps = [injector.get(dep) for dep in dependencies]
            return func(*a, *deps, **kw)
        return _func
    return wrapper
