from helpers.loggers import getlogger
import json
from helpers import path, sha1OfFile

file_logger = getlogger ( 'files', 'Helpers', level = 'WARNING' )

def get_file_hash( filepath, hash_func = sha1OfFile, default = None ):
    try: 
        return hash_func( filepath )
    except FileNotFoundError:
        file_logger.warning( "Couldn't compute hash `{}` for file `{}` because the file doesn't exist!".format( hash_func.__name__, filepath ) )
        return default


def get_from_file( file, designated_type = None ):
    try:
        with file as f:
            response_item = json.load( f )
        if designated_type is None or isinstance( response_item, designated_type ):
            return response_item
        else:
            raise TypeError( '`{}` is not of type: {}'.format( str( response_item )[:1000], designated_type ) )
    except json.decoder.JSONDecodeError:
        file_logger.exception( '{}: {}'.format( file.name, response.content ) )
        raise


def get_file( filepath, *args, **kwargs):
    try:
        return open( filepath, *args, **kwargs )
    except Exception as e:
        file_logger.exception( filepath )
        raise


def get_file_config( filepath, hash_func ):
    config_dict = get_from_file ( get_file( filepath ), dict )
    config_hash = get_file_hash( filepath, hash_func )
    return config_dict, config_hash
