from helpers import md5

def is_updatable( obj ):
    for method_name in ( '_update', 'update', '_preupdate', '_postupdate', 'preupdate', 'postupdate', 'get_remote_hash', 'get_local_hash', 'populate', 'remote_hash', 'local_hash' ):
        if not hasattr( obj, method_name ):
            print( 'it aint have', method_name )
            return False
    return True


class Updatable( object ):
    """Semi abstract class defining necessary methods for other classes that are updated"""

    def __init__( self ):
        self.remote_hash = None
        self.local_hash = None
        self._update()

    def _update( self ):
        self.preupdate()
        new_hash = self.get_remote_hash()
        if new_hash != self.remote_hash:
            self.update()
        self.local_hash = self.get_local_hash()
        self.postupdate()

    def _preupdate( self ):
        self.preupdate()

    def preupdate( self ):
        pass

    def _postupdate( self ):
        self.postupdate()

    def postupdate( self ):
        pass

    def get_remote_hash( self ):
        NotImplemented

    def get_local_hash( self ):
        NotImplemented

    def populate( self ):
        NotImplemented

    def update( self ):
        NotImplemented


class Subscriptable( Updatable ):

    def __init__( self ):
        super().__init__()
        self.subscribed = {}
        self.hashcache = None
        if not hasattr( self, 'config' ):
            self.config = None

    def subscribe( self, updatable, required_attrs ):
        if updatable in self.subscribed:
            print( '{} already subscribed to: {}'.format( updatable, self ) )
        self.subscribed[ updatable ] = required_attrs

    def unsubscribe( self, updatable ):
        if updatable in self.subscribed:
            del self.subscribed[ updatable ]
        else:
            raise AttributeError( 'Updatable {} was not subscribed to {} in the first place!!!'.format( updatable, self ) )

    def _preupdate( self ):
        self.hashcache = self.local_hash
        super()._preupdate()

    def _postupdate( self ):
        if self.hashcache != self.local_hash:
            self.update_subscribed()
        super()._postupdate()

    def update_subscribed( self ):
        for subscribed, required_attrs in self.subscribed.items():
            subscribed.update(
                updated_required = {
                    required_attr: getattr( self, required_attr, None ) if required_attr != 'self' else self
                        for required_attr in required_attrs
                }
            )


class ItemStoreConfig(Subscriptable):

    def __init__(self, url):
        from pytemstore import ItemStore
        self.is_pure = ItemStore.pure(url)
        super().__init__()

    def populate(self, config):
        pass
        # self.config = config

    def get_remote_hash(self):
        return self.is_pure.head()

    def get_local_hash(self):
        return md5(str(self.config))

    def update(self):
        response = self.is_pure.get()
        print(response)
        item = response.items[0]
        self.populate(item.item)
