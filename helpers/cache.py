import time
from functools import wraps
from typing import Dict, Any, Tuple, Callable
from .typing import DictItems
ArgVal = Tuple[Any, float]
ArgCache = Dict[Tuple, ArgVal]
FuncCache = Dict[Callable, ArgCache]


class MemoizeWithTimeout:
    _caches: FuncCache = {}
    _timeouts: Dict[Callable, float] = {}

    def __init__(self, timeout = 60) -> None:
        self.timeout: float = float(timeout)

    def collect(self) -> None:
        """Clear cache of results which have timed out"""
        for func in self._caches:
            cache: ArgCache = {}
            for key in self._caches[ func ]:
                if ( time.time() - self._caches[ func ][ key ][ 1 ] ) < self._timeouts[ func ]:
                    cache[ key ] = self._caches[ func ][ key ]
            self._caches[ func ] = cache

    def __call__( self, f ) -> Any:
        self.cache: ArgCache
        self.cache = self._caches[ f ] = {}
        self._timeouts[ f ] = self.timeout
        @wraps( f )
        def func( *args, **kwargs ):
            kw: DictItems = sorted(kwargs.items())
            key: Tuple = (args, tuple(kw))
            try:
                v: Tuple[Any, float] = self.cache[ key ]
                if ( time.time() - v[ 1 ] ) > self.timeout:
                    raise KeyError
            except KeyError:
                v = self.cache[ key ] = f( *args, **kwargs ), time.time()
            return v[ 0 ]
        return func


def create_minute_memoize( no_minutes: float ) -> Callable:
    return MemoizeWithTimeout( no_minutes * 60 )


minute_memoize: Callable = MemoizeWithTimeout( 60 )
cache_for: Callable = create_minute_memoize
memoize_for: Callable = create_minute_memoize


_remember_cache = {}
def remember(func):
    @wraps(func)
    def _wrap(*a, **kw):
        if str(type(func)) in ("<class 'function'>", "<class 'builtin_function_or_method'>"):
            obj = None
        elif str(type(func)) == "<class 'method'>":
            obj = a[0]
        if obj not in _remember_cache:
            _remember_cache[obj] = {}
        if func not in _remember_cache[obj]:
            _remember_cache[obj][func] = func(*a, **kw)
        return _remember_cache[obj][func]
    return _wrap
