from helpers.helper import Date
import re
from datetime import datetime

def is_valid_slug(slug):
    try:
        int(slug)
        return True
    except ValueError:
        pass
    split = re.findall(r"[^\-_\.]+", slug)
    if len(split) < 4:
        if 'article.cfm' in slug:
            return True
        return False
    return True

def is_sitemap(line):
    if not 'Sitemap: ' in line:
        return False
    last_slug = line[line.rfind('/'):]
    #if 'news' in last_slug:
    #    return True
    if 'siteMapNews' in last_slug:
        return True
    if 'google' in last_slug:
        return True
    if 'article' in last_slug:
        return True
    if 'cmlink' in line:
        return True
    if '/sitemap_index.xml' in line:
        return True
    return False

sitemap_link_date_regexes = [{'regex': '\d\d-\d\d-\d\d\d\d', 'format': '%d-%m-%Y'}, {'regex': '\d\d\d\d-\d{1,2}', 'format': '%Y-%W-%w', 'process': lambda x: x+'-0'}]

def find_date_in_sitemap_link(text):
    dates = []
    for regex in sitemap_link_date_regexes:
        matches = re.findall(regex['regex'], text)
        for match in matches:
            match = regex['process'](match) if 'process' in regex else match
            dates.append(datetime.strptime(match, regex['format']))
    dates.sort()
    return Date(dates[-1]) if len(dates) > 0 else None
