from collections.abc import Sequence, Mapping

class ReadOnlyAttribute( object ):
    def __init__( self, name, default = None ):
        self.name = name

    def __get__( self, instance, owner ):
        if self.name in instance._storage:
            return instance._storage[ self.name ]

    def __set__( self, instance, value ):
        raise ChangingImmutableProperty( 'Can\'t change {} of {}!!!'.format( self.name, instance.type ) )


class LinkedObject( object ):

    def __getitem__( self, path ):
        if type( path ) is tuple:
            item = self._storage
            for path_elem in path:
                item = item[ path_elem ]
            return item
        else:
            return self._storage[ path ]


    def get( self, path, default = None ):
        try:
            self[ path ]
        except KeyError:
            return default


    def __setitem__( self, path, value ):
        if type( path ) is tuple:
            item = self._storage
            for i, path_elem in enumerate( path ):
                if i + 1 == len( path ):
                    item[ path_elem ] = value
                else:
                    item = item[ path_elem ]
        else:
            self._storage[ path ] = value


    def __iter__( self ):
        return iter( self._storage )


    def __len__( self ):
        return len( self._storage )


    def __str__( self ):
        return str( self._storage )


    def __repr__( self ):
        return self.__str__()


class List( LinkedObject, Sequence ):

    def __init__( self, item_list ):
        self._storage = list( item_list )
        for i, item in enumerate( self._storage ):
            if type( item ) is list:
                self._storage[ i ] = List( item )
            elif type( item ) is dict:
                self._storage[ i ] = Dictionary( item )

    def to_python( self ):
        return [
            elem.to_python() if type( elem ) in ( List, Dictionary ) else elem for elem in self._storage
        ]


class Dictionary( LinkedObject, Mapping ):

    def __init__( self, item_dict ):
        self._storage = dict( **item_dict )
        for key, value in self._storage.items():
            if type( value ) is list:
                self._storage[ key ] = List( value )
            elif type( value ) is dict:
                self._storage[ key ] = Dictionary( value )


    def to_python( self ):
        return {
            key: value.to_python() if type( value ) in ( List, Dictionary ) else value for key, value in self._storage.items()
        }
