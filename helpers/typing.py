from typing import Union, List, Tuple, Any, Callable
Number = Union[int, float]
DictItems = List[Tuple[Any, Any]]

ByteLike = Union[str, bytes]
HashFunction = Callable[[ByteLike], int]
HexHashFunction = Callable[[ByteLike], str]

KeyValue = Tuple[Any, Any]
