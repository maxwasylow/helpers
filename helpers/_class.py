'''Useful helper classes'''
from pprint import pprint, pformat
import simplejson as json


class ToDict:
    '''
        ToDict implements to_dict method which
        returns a dictionary representation of
        an inheriting class instance
        Class level attributes:
            - fields: list of fields to be included in dict representation
    '''
    def __init__(self):
        self._dict = {}

    def __setattr__(self, attr, value):
        if attr != '_dict':
            self._dict[attr] = value
        super().__setattr__(attr, value)

    def __str__(self) -> str:
        return str(self.to_dict())

    def to_dict(self) -> dict:
        '''Returns dict representation of instance'''
        return self._dict


class StrictToDict(ToDict):
    '''Same as ToDict but allowing dict keys to be only from _fields'''
    _fields = []
    def __setattr__(self, attr, value):
        if attr in self._fields:
            self._dict[attr] = value
        object.__setattr__(self, attr, value)


class ToJson(ToDict):
    '''
        ToJson class implements to_json method which takes
        a dict representation of an inheriting class (self.to_dict())
        and converts it to json encoded string
    '''
    def to_json(self, **kw) -> str:
        '''Converts self.to_dict() to json string'''
        return json.dumps(self.to_dict(), **kw)


class PPrintable(ToDict):
    '''
        PPrintable implements pprint and pformat
        And changes behaviour of __str__ and __repr__ to use pformat
    '''
    def pprint(self, *a, **kw):
        '''look: pprint.pprint'''
        pprint(self.to_dict(), *a, **kw)

    def pformat(self, *a, **kw):
        '''look: pprint.pformat'''
        return pformat(self.to_dict(), *a, **kw)

    def __str__(self):
        return self.pformat()

    def __repr__(self):
        return str(self)
