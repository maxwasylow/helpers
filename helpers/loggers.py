import watchtower, logging
from boto3.session import Session


def getlogger(name, log_group, level = 'INFO', cloudwatch_level = 'DEBUG'):
    level = logging.__dict__[level]
    cloudwatch_level = logging.__dict__[cloudwatch_level]
    
    logger = logging.getLogger(name)

    if not logger.handlers:

        formatter = logging.Formatter(
                '[%(asctime)s.%(msecs)03d] [%(threadName)s] [%(levelname)s] [%(name)s.%(funcName)s:%(lineno)d] %(message)s',
                '%d-%m-%Y %H:%M:%S'
            )

        cloudwatch = watchtower.CloudWatchLogHandler(log_group = log_group, boto3_session = Session(region_name = 'eu-west-1'), use_queues = True, max_batch_size = 838860)
        cloudwatch.setLevel(cloudwatch_level)
        cloudwatch.setFormatter(formatter)

        console = logging.StreamHandler()
        console.setLevel(level)
        console.setFormatter(formatter)

        logger.addHandler(cloudwatch)
        logger.addHandler(console)

    logger.setLevel(logging.DEBUG)

    return logger
