from helpers import ts

def benchmark( *fs, n = 1000000 ):
    for fparams in fs:
        args = []
        kwargs = {}
        desc = ''
        if hasattr( fparams, '__call__' ) or len( fparams ) == 1:
            f = fparams
        elif len( fparams ) == 2:
            f = fparams[ 0 ]
            args = fparams[ 1 ]
        elif len( fparams ) == 3:
            f = fparams[ 0 ]
            args = fparams[ 1 ]
            kwargs = fparams[ 2 ]
        else:
            f = fparams[ 0 ]
            args = fparams[ 1 ]
            kwargs = fparams[ 2 ]
            desc = fparams[ 3 ]
        t1 = ts( precision = True )
        for i in range( n ):
            res = f( *args, **kwargs )
        t2 = ts( precision = True )
        print( '{} \'{}\' ({} samples) run for: {}s'.format( f.__name__, desc, n, t2 - t1 ) )

STR64 = 'd&sjz,c^@dat45NGAjF#fM-FcYc%!"B-"7o0Vh40tW:F?VcU282/f>"m[YGtO3~'
STR = {
    64: STR64,
    32: STR64[ :32 ],
    16: STR64[ :16 ],
    8: STR64[ :8 ],
    4: STR64[ :4 ]
}

def sample_s_add( n ):
    return {
        1: lambda a: a,
        2: lambda a, b: a + b,
        3: lambda a, b, c: a + b + c,
        4: lambda a, b, c, d: a + b + c + d,
        5: lambda a, b, c, d, e: a + b + c + d + e,
        6: lambda a, b, c, d, e, f: a + b + c + d + e + f
    }[ n ]

def sample_s_format( n ):
    return {
        1: lambda a: '{}'.format( a ),
        2: lambda a, b: '{}{}'.format( a, b ),
        3: lambda a, b, c: '{}{}{}'.format( a, b, c ),
        4: lambda a, b, c, d: '{}{}{}{}'.format( a, b, c, d ),
        5: lambda a, b, c, d, e: '{}{}{}{}{}'.format( a, b, c, d, e ),
        6: lambda a, b, c, d, e, f: '{}{}{}{}{}{}'.format( a, b, c, d, e, f )
    }[ n ]

if __name__ == '__main__':
    fs = [ ( sample_s_add( i ), [ STR64 ] * i, {}, 'Add( {} )'.format( i) ) for i in range( 1, 7 ) ] +\
         [ ( sample_s_format( i ), [ STR64 ] * i, {}, 'Format( {} )'.format( i ) ) for i in range( 1, 7 ) ]
    benchmark( *fs )
