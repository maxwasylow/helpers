from helpers import vertical_bar

def customized_print( *args, **kwargs ):
    bar = None
    if 'bars' in kwargs:
        bars = kwargs[ 'bars' ]
        del kwargs[ 'bars' ]
        if type( bars ) is tuple:
            prebar, postbar = bars
        else:
            prebar = postbar = bars
        vertical_bar( prebar )
        old_print( *args, **kwargs )
        vertical_bar( postbar )
    else:
        old_print( *args, **kwargs )
old_print = print
print = customized_print


def setup_script( globals ):
    globals[ 'old_print' ] = old_print
    globals[ 'print' ] = print
