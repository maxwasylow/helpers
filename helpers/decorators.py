'''Module containing various decorators'''
from functools import wraps


def aslist(func):
    '''Converts generator function into a function that returns list'''
    @wraps(func)
    def _wrapper(*a, **kw):
        return list(func(*a, **kw))
    return _wrapper

def asdict(func):
    '''
        Converts generator function into a function that returns dict
        Wrapped function must generate a key-value tuples
    '''
    @wraps(func)
    def _wrapper(*a, **kw) -> dict:
        return dict(func(*a, **kw))
    return _wrapper

def log_return(func):
    '''Runs a wrapped function and logs its return value'''
    @wraps(func)
    def _wrapper(*a, **kw):
        return_val = func(*a, **kw)
        print(return_val)
        return return_val
    return _wrapper
