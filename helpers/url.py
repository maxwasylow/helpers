def make_url(url: str, params: dict) -> str:
    if not params:
        return url
    param_str = '&'.join(f'{k}={v}' for k, v in sorted(params.items(), key=lambda x: x[0]))
    return f'{url}?{param_str}'
