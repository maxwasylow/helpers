from contextlib import contextmanager
from typing import Sequence
import inspect
import os
_dir_stack: Sequence[str] = []


@contextmanager
def cd_to_script_location():
    file_dirname = get_dirname(1)
    pushd(file_dirname)
    yield file_dirname
    popd()

def pushd(dirname: str):
    _dir_stack.append(os.getcwd())
    os.chdir(dirname)

def popd():
    dirname: str = _dir_stack.pop()
    os.chdir(dirname)

_bad_read_flags = set([*'wxa'])
def read(filename: str, flags: str=''):
    if 'b' not in flags:
        flags += 't'
    flagset = set([*flags])
    if flagset.intersection(_bad_read_flags) != set():
        raise ValueError(f"Flags {flags} can't be used to read a file!")
    flagset.add('r')
    flags = ''.join(flagset)
    with open(filename, flags) as f:
        return f.read()

def get_dirname(__reset_stack=0, last=False):
    if is_interactive():
        dirname = os.getcwd()
    else:
        caller_stack = inspect.stack()[2+__reset_stack]
        caller_module = inspect.getmodule(caller_stack[0])
        caller_file = caller_module.__file__
        file_realname = os.path.realpath(caller_file)
        dirname = os.path.dirname(file_realname)
    if last:
        return dirname.split(os.path.sep)[-1]
    else:
        return dirname

def is_interactive():
    import __main__ as main
    return not hasattr(main, '__file__')
