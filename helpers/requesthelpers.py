from helpers.loggers import getlogger
import json
import helpers.requestwrapper as requests
from helpers import path

request_logger = getlogger ( 'requests', 'Helpers', level = 'WARNING' )

def get_header_from_response( response, header, default = None ):
    if header in response.headers:
        return response.headers[ header ]
    else:
        request_logger.warning( "No {} header in response from: `{}`!".format( hash_header, response.url ) )
        return default


def get_from_response(response, designated_type=None):
    try:
        response_item = json.loads(response.content)
        if designated_type is None or isinstance(response_item, designated_type):
            return response_item
        else:
            raise TypeError( '`{}` is not of type: {}'.format( str( response_item )[:1000], designated_type ) )
    except json.decoder.JSONDecodeError:
        request_logger.exception( '{}: {}'.format( response.url, response.content ) )
        raise


def get_response( request, *args, allowed = [ 200 ], **kwargs ):
    try:
        response = request(*args, **kwargs)
        if response.status_code not in allowed:
            request_logger.warning( '{}: Bad response for {}({})'.format( response, request.__name__ ) )
        else:
            return response
    except Exception as e:
        request_logger.exception( args[0] )
        raise


def get_remote_config( url, hash_header, json_path = [] ):
    response = get_response ( requests.get, url )
    config_dict = path( get_from_response ( response, ( dict, list ) ), json_path )
    config_hash = get_header_from_response( response, hash_header )
    return config_dict, config_hash


def get_remote_header( url, header ):
    return get_header_from_response( get_response( requests.head, url ), header )
