from datetime import datetime
from scrapy.exceptions import CloseSpider

def spider_opened(spider):
    spider.start = datetime.now()

def spider_closed(spider):
    if not spider.successful():
        spider.reject()
    else:
        spider.accept()
    spider.log()

def item_scraped(item, response, spider):
    spider.successes.append((datetime.now(), item['url']))
    if len(spider.successes) > 500:
        raise CloseSpider('500 items scraped!!! Closing spider...')

def item_dropped(item, response, exception, spider):
    spider.failures.append((datetime.now(), item['url'], exception))

def spider_error(failure, response, spider):
    print('Error in spider_error')
    import code; code.interact(local=locals())
    spider.errors.append((datetime.now(), failure, response))
