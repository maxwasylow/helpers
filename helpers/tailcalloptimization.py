import sys

import helpers


class TailRecurseException(Exception):
    def __init__(self, args, kwargs):
        self.args = args
        self.kwargs = kwargs

def tail_call_optimized(g):
    """
        This function decorates a function with tail call
        optimization. It does this by throwing an exception
        if it is it's own grandparent, and catching such
        exceptions to fake the tail call optimization.

        This function fails if the decorated
        function recurses in a non-tail context.
    """
    def func(*args, **kwargs):
        f = sys._getframe()
        if f.f_back and f.f_back.f_back and f.f_back.f_back.f_code == f.f_code:
            raise TailRecurseException(args, kwargs)
        else:
            while 1:
                try:
                    return g(*args, **kwargs)
                except TailRecurseException as e:
                    args = e.args
                    kwargs = e.kwargs
    func.__doc__ = g.__doc__
    return func

@tail_call_optimized
def factorial(n, acc=1):
    "calculate a factorial"
    if n == 0:
        return acc
    return factorial(n-1, n*acc)

@tail_call_optimized
def fib(i, current=0, next=1):
    if i == 0:
        return current
    else:
        return fib(i - 1, next, current + next)

def nfib(i, current=0, next=1):
    if i == 0:
        return current
    else:
        return nfib(i - 1, next, current + next)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
    else:
        n = 1000
    start = helpers.ts(precision=True)
    x = fib(n)
    end = helpers.ts(precision=True)
    try:
        y = nfib(n)
    except RecursionError:
        print(f'Non TCO fibonacci failed!!!')
    else:
        end2 = helpers.ts(precision=True)
        print(f'Fibonacci no TCO {end2-end:.12f} seconds')
        print(f'Equal? {y == x}')
    print(f'Fibonacci TCO {end-start:.12f} seconds')
    print(f'Fibonacci of {n} is {x}')

