from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from functools import wraps
import requests
import logging
logger = logging.getLogger(__name__)


def restart_on_connerr(func):
    @wraps(func)
    def _run(self, *a, **kw):
        retries = 0
        while retries < 2:
            try:
                return func(self, *a, **kw)
            except ConnectionError:
                logger.exception('Restarting on ConnectionError')
                self.session = requests.Session()
                self.update_retries()
                time.sleep(2**retries)
                retries += 1
        else:
            return func(self, *a, **kw)
    return _run

def make_get_set(prop_name):
    def getter(self):
        return getattr(self, f'_{prop_name}')
    getter.__name__ = f'{prop_name}_getter'
    getter.__doc__ = f'{prop_name} getter'
    def setter(self, value):
        setattr(self, f'_{prop_name}', value)
        self.update_retries()
    setter.__name__ = f'{prop_name}_setter'
    setter.__doc__ = f'{prop_name} setter'
    return property(getter, setter)


class Requests:
    DEFAULT_STATUS_FORCELIST = frozenset((400, 403, 500, 502, 503, 504))
    update_on_change = ('max_retries', 'retry_on', 'accepted_on', 'backoff_factor', 'raise_on_status')
    def __init__(self, max_retries=5, retry_on=None, accepted_on=None, backoff_factor=1, raise_on_status=False):
        if accepted_on is None:
            accepted_on = set()
        if retry_on is None:
            retry_on = set()
        for method_name in self.update_on_change:
            setattr(self.__class__, method_name, make_get_set(method_name))
        self._retry_on = retry_on
        self._accepted_on = accepted_on
        self._max_retries = max_retries
        self._backoff_factor = backoff_factor
        self._raise_on_status = raise_on_status
        self.session = requests.Session()
        self.update_retries()

    def update_retries(self) -> None:
        status_forcelist = frozenset(code for code in (list(self.DEFAULT_STATUS_FORCELIST) + list(self.retry_on)) if code not in self.accepted_on)
        retries = Retry(
            total = self.max_retries,
            connect = 0,
            backoff_factor = self.backoff_factor,
            status_forcelist = status_forcelist,
            raise_on_status = self.raise_on_status
        )
        adapter = HTTPAdapter(pool_connections=50, pool_maxsize=100, max_retries=retries)
        self.session.mount('http://', adapter)
        self.session.mount('https://', adapter)

    @restart_on_connerr
    def get(self, *a, **kw):
        return self.session.get(*a, **kw)

    @restart_on_connerr
    def head(self, *a, **kw):
        return self.session.head(*a, **kw)

    @restart_on_connerr
    def delete(self, *a, **kw):
        return self.session.delete(*a, **kw)

    @restart_on_connerr
    def post(self, *a, **kw):
        return self.session.post(*a, **kw)

    @restart_on_connerr
    def put(self, *a, **kw):
        return self.session.put(*a, **kw)
