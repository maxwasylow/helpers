import logging
from copy import deepcopy
logger = logging.getLogger( __name__ )

def path( path, dct, default = ... ):
    if path is None:
        return default
    if type( path ) not in (list, tuple):
        raise TypeError( 'Path argument must be a sequence!' )
    tmp = dct
    for field in path:
        try:
            tmp = tmp[ field ]
        except ( KeyError, IndexError, TypeError ):
            if default is ...:
                logger.exception( f'The object doesnt have field {field} [path: {path}]' )
            else:
                return default
    return tmp


def set_path(path, dct, value, inplace = True):
    if path is None or type(path) not in (list, tuple):
        raise TypeError(f'Path argument must be a sequence!')
    if not inplace:
        tmp = deepcopy(dct)
    else:
        tmp = dct
    for field in path[:-1]:
        tmp = tmp[field]
    tmp[path[-1]] = value
    return tmp


# class MultiDict(collections.MutableMapping):
#     """
#         A dictionary that takes series as keys and creates a mapping for each element
#     """
# 
#     def __init__(self, *args, **kwargs):
#         self.store = dict()
#         self.update(dict(*args, **kwargs))  # use the free update to set keys
# 
#     def __getitem__(self, key):
#         return self.store[self.__keytransform__(key)]
# 
#     def __setitem__(self, key, value):
#         self.store[self.__keytransform__(key)] = value
# 
#     def __delitem__(self, key):
#         del self.store[self.__keytransform__(key)]
# 
#     def __iter__(self):
#         return iter(self.store)
# 
#     def __len__(self):
#         return len(self.store)
# 
#     def __keytransform__(self, key):
#         return key
